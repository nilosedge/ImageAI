package net.nilosplace.imageai.repo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.nilosplace.imageai.entity.node.Picture;
import net.nilosplace.imageai.util.Neo4jRepository;

public class PictureRepository extends Neo4jRepository<Picture> {
	private final Logger log = LogManager.getLogger(getClass());
	public PictureRepository() {
		super(Picture.class);
	}
}