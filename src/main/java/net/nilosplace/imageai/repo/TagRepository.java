package net.nilosplace.imageai.repo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.nilosplace.imageai.entity.node.Tag;
import net.nilosplace.imageai.util.Neo4jRepository;

public class TagRepository extends Neo4jRepository<Tag> {
	private final Logger log = LogManager.getLogger(getClass());
	public TagRepository() {
		super(Tag.class);
	}
}