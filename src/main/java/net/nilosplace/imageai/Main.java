package net.nilosplace.imageai;

import java.net.URI;
import java.util.HashMap;

import net.nilosplace.imageai.s3.S3ObjectLister;
import net.nilosplace.imageai.service.PictureService;
import net.nilosplace.imageai.util.ConfigHelper;

public class Main {

	public static void main(String[] args) throws Exception {
		ConfigHelper.init();

		PictureService service = new PictureService();

		//service.faceDetection("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0026.JPG");

		S3ObjectLister lister = new S3ObjectLister();

		HashMap<String, URI> map = lister.getList();

		System.out.println(map.size());

		int c = 0;
		for(String key: map.keySet()) {
			try {
				System.out.println(key + " -> " + map.get(key).toASCIIString());
				service.addInput(map.get(key).toASCIIString());
				if(++c > 10000) break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/clear_bottle.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0003.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0005.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0006.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0007.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0008.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0009.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0010.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0011.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0012.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0013.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0014.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0015.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0016.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0017.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0018.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0019.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0020.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0021.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0022.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0023.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0024.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0025.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0026.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0027.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0028.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0029.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0030.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0031.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0032.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0033.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0034.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0035.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0036.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0037.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0039.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0040.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0041.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0042.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0043.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0044.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0045.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0046.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0047.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0048.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0053.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0054.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0055.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0056.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0057.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0058.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0059.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0060.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0061.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/Pict0062.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/pict5.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/stefan.JPG");
//		service.addInput("http://oblodget.nilosplace.net/pix/mashchaks/second/vit_E.JPG");


	}
}
