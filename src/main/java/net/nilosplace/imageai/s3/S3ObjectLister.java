package net.nilosplace.imageai.s3;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.regex.Pattern;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3ObjectLister {

	private final AmazonS3 s3client;

	public S3ObjectLister() {
		s3client = new AmazonS3Client(new ProfileCredentialsProvider("nilosplace"));
	}

	public HashMap<String, URI> getList() throws Exception {

		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName("backupcameraphotos").withMaxKeys(1000);
		ListObjectsV2Result result;

		HashMap<String, URI> map = new HashMap<String, URI>();

		do {
			result = s3client.listObjectsV2(req);

			// IMG_0897.jpg

			for (S3ObjectSummary objectSummary: result.getObjectSummaries()) {
				File f = new File(objectSummary.getKey());

				String fileName = f.getName().toLowerCase();

				//System.out.println(objectSummary.getKey());
				boolean found = false;
				if(Pattern.compile("img_\\d{4}.jpg").matcher(fileName).matches() || Pattern.compile("dscn\\d{4}.jpg").matcher(fileName).matches()) {
					found = true;
					//map.put("http://backupphotos.nilosplace.net/" + objectSummary.getKey(), new URL("http://backupphotos.nilosplace.net/" + objectSummary.getKey()));
					map.put(fileName, new URI("http", "backupphotos.nilosplace.net", "/" + objectSummary.getKey(), null));
				}
			}

			//if(list.size() >= 10000) break;

			//System.out.println("Next Continuation Token : " + result.getNextContinuationToken());
			req.setContinuationToken(result.getNextContinuationToken());
		} while(result.isTruncated() == true );

		return map;
	}
}
