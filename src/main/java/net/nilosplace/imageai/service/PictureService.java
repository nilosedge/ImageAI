package net.nilosplace.imageai.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.api.request.input.AddInputsRequest;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Concept;
import net.nilosplace.imageai.entity.node.Picture;
import net.nilosplace.imageai.entity.node.Tag;
import net.nilosplace.imageai.entity.relationship.PictureTag;
import net.nilosplace.imageai.repo.PictureRepository;
import net.nilosplace.imageai.repo.TagRepository;

public class PictureService {

	private final PictureRepository prepo = new PictureRepository();
	private final TagRepository trepo = new TagRepository();
	private final ClarifaiClient client = new ClarifaiBuilder("API ID HERE").buildSync();
	private final Logger log = LogManager.getLogger(getClass());

	public void addInput(String imageURL) {
		AddInputsRequest inputRequest = client.addInputs();
		inputRequest.plus(
			ClarifaiInput.forImage(imageURL)
		).executeSync();
	}

	public void addInputs(List<String> imageURLs) {
		AddInputsRequest inputRequest = client.addInputs();
		for(String url: imageURLs) {
			inputRequest.plus(ClarifaiInput.forImage(url));
		}
		inputRequest.executeSync();
	}

	public void updateImage(String imageURL) {

		try {
			final List<ClarifaiOutput<Concept>> predictionResults = client.getDefaultModels().generalModel().predict()
					.withInputs(ClarifaiInput.forImage(imageURL))
					.executeSync().get();

			Picture p = new Picture();
			p.setUrl(imageURL);

			for(ClarifaiOutput<Concept> con: predictionResults) {
				log.info(con.id());
				p.setPictureId(con.id());
				for(Concept concept: con.data()) {
					Tag t = new Tag();
					t.setName(concept.name());
					t.setTagId(concept.id());
					trepo.save(t);
					PictureTag pt = new PictureTag();
					pt.setPicture(p);
					pt.setTag(t);
					pt.setValue(concept.value());
					p.getPictureTags().add(pt);
					log.info(concept.name() + " (" + concept.id() + ") -> " + concept.value());
				}
			}

			prepo.save(p);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void faceDetection(String string) {


	}

}
