package net.nilosplace.imageai.entity.node;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import lombok.Getter;
import lombok.Setter;
import net.nilosplace.imageai.entity.Entity;
import net.nilosplace.imageai.entity.relationship.PictureTag;

@Getter @Setter
@NodeEntity
public class Picture extends Entity {

	@Index(unique=true, primary=true)
	private String pictureId;
	private String url;

	@Relationship(type="PICTURE_TAG")
	private List<PictureTag> pictureTags = new ArrayList<PictureTag>();
}
