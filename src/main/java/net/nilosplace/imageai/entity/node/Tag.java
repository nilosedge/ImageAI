package net.nilosplace.imageai.entity.node;

import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;

import lombok.Getter;
import lombok.Setter;
import net.nilosplace.imageai.entity.Entity;

@Getter @Setter
@NodeEntity
public class Tag extends Entity {

	@Index(unique=true, primary=true)
	private String tagId;

	private String name;
}
