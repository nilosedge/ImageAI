package net.nilosplace.imageai.entity.relationship;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import lombok.Getter;
import lombok.Setter;
import net.nilosplace.imageai.entity.Entity;
import net.nilosplace.imageai.entity.node.Picture;
import net.nilosplace.imageai.entity.node.Tag;

@Getter @Setter
@RelationshipEntity(type="PICTURE_TAG")
public class PictureTag extends Entity {

	@StartNode
	private Picture picture;
	@EndNode
	private Tag tag;
	private Float value;

}
